onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 DUT
add wave -noupdate -divider {output PRBS}
add wave -noupdate /sisr_tb/vectorO2_s
add wave -noupdate /sisr_tb/tick0p5Hz_s
add wave -noupdate -divider {generate PRBS}
add wave -noupdate /sisr_tb/vectorO1_s
add wave -noupdate /sisr_tb/tick4Hz_s
add wave -noupdate -divider Display
add wave -noupdate /sisr_tb/seg7a_s
add wave -noupdate /sisr_tb/seg7c_s
add wave -noupdate /sisr_tb/tick1KHz_s
add wave -noupdate -divider CLocK
add wave -noupdate /sisr_tb/clk
add wave -noupdate -divider SISR
add wave -noupdate /sisr_tb/vectorO2_s
add wave -noupdate /sisr_tb/tick0p5Hz_s
add wave -noupdate -divider output
add wave -noupdate /sisr_tb/vectorO1_s
add wave -noupdate /sisr_tb/tick4Hz_s
add wave -noupdate /sisr_tb/sisr_i1/PRBS_s
add wave -noupdate /sisr_tb/seg7a_s
add wave -noupdate /sisr_tb/seg7c_s
add wave -noupdate /sisr_tb/tick1KHz_s
add wave -noupdate /sisr_tb/sisr_i1/signature
add wave -noupdate /sisr_tb/clk
add wave -noupdate /sisr_tb/sisr_i1/ticki
add wave -noupdate -divider flipflops
add wave -noupdate /sisr_tb/sisr_i1/ff_ns
add wave -noupdate /sisr_tb/sisr_i1/ff_cs
add wave -noupdate /sisr_tb/sisr_i1/clk
add wave -noupdate /sisr_tb/sisr_i1/sres
add wave -noupdate -divider -height 20 DISPLAYMUX
add wave -noupdate -divider {data in}
add wave -noupdate /sisr_tb/display/value
add wave -noupdate -divider flipflop
add wave -noupdate /sisr_tb/display/digit_cs
add wave -noupdate /sisr_tb/display/digit_ns
add wave -noupdate -divider {signals out}
add wave -noupdate /sisr_tb/display/seg7a
add wave -noupdate /sisr_tb/display/seg7a_s
add wave -noupdate /sisr_tb/display/seg7c
add wave -noupdate /sisr_tb/display/seg7c_s
add wave -noupdate -divider {clocks & ticki}
add wave -noupdate /sisr_tb/display/ticki
add wave -noupdate /sisr_tb/display/clk
add wave -noupdate /sisr_tb/display/sres
add wave -noupdate -divider -height 20 TICKER
add wave -noupdate /sisr_tb/tick0p5Hz/ticko
add wave -noupdate /sisr_tb/tick0p5Hz/count_cs
add wave -noupdate /sisr_tb/tick0p5Hz/ticki
add wave -noupdate /sisr_tb/tick4Hz/ticko
add wave -noupdate /sisr_tb/tick4Hz/count_cs
add wave -noupdate /sisr_tb/tick4Hz/ticki
add wave -noupdate /sisr_tb/tick1KHz/ticko
add wave -noupdate /sisr_tb/tick1KHz/count_cs
add wave -noupdate /sisr_tb/tick1KHz/ticki
add wave -noupdate /sisr_tb/stimuli/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {221 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {1888 ns}
