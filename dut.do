onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 DUT
add wave -noupdate -divider {output PRBS}
add wave -noupdate /sisr_tb/vectoro2_s
add wave -noupdate /sisr_tb/tick0p5hz_s
add wave -noupdate -divider PRBS
add wave -noupdate /sisr_tb/vectoro1_s
add wave -noupdate /sisr_tb/tick4hz_s
add wave -noupdate -divider Display
add wave -noupdate /sisr_tb/seg7a_s
add wave -noupdate /sisr_tb/seg7c_s
add wave -noupdate /sisr_tb/tick1khz_s
add wave -noupdate -divider led
add wave -noupdate /sisr_tb/led
add wave -noupdate -divider CLocK
add wave -noupdate /sisr_tb/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {3189 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {2997 ns} {3923 ns}
