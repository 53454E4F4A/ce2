onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 DISPLAYMUX
add wave -noupdate -divider {data in}
add wave -noupdate /sisr_tb/display/value
add wave -noupdate -divider flipflop
add wave -noupdate /sisr_tb/display/digit_cs
add wave -noupdate /sisr_tb/display/digit_ns
add wave -noupdate -divider {signals out}
add wave -noupdate /sisr_tb/display/seg7a
add wave -noupdate /sisr_tb/display/seg7a_s
add wave -noupdate /sisr_tb/display/seg7c
add wave -noupdate /sisr_tb/display/seg7c_s
add wave -noupdate -divider {clocks & ticki}
add wave -noupdate /sisr_tb/display/ticki
add wave -noupdate /sisr_tb/display/clk
add wave -noupdate /sisr_tb/display/sres
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
WaveRestoreCursors {{Cursor 1} {1784 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 120
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {97642 ns} {102835 ns}
