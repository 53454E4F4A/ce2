library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;
    
    
    
    
    
entity sisr_tb is
end entity sisr_tb;





architecture beh of sisr_tb is
    
    constant 	poly   		: std_logic_vector(16 downto 0) := "11010000000010001";	-- prbs generator polynom
    
    
    signal 		sres      	: std_logic;	-- synchronous reset
    signal 		clk       	: std_logic;	-- clock
     
    signal		vectoro1_s  : std_logic_vector(poly'high downto 1);	-- currently generated prbs
	signal 		vectoro2_s  : std_logic_vector(poly'high downto 1);	-- prbs displayed on the display
	
	signal 		tick0p5hz_s : std_logic;	-- enables next output
	signal		tick4hz_s   : std_logic;	-- enables prbs generation
	signal		tick1khz_s 	: std_logic;	-- display refresh rate
	signal		seg7a_s	   	: std_logic_vector(3 downto 0);	-- 4 digits
	signal		seg7c_s 	: std_logic_vector(7 downto 0);	-- 8 segments
    signal 		led 		: std_logic_vector(7 downto 0);	-- 8 leds
	
	

	
    component stimuliGen is
		port (
		clk	: out	std_logic;
		res	: out	std_logic
		);
	end component stimuligen;
	for all : stimuliGen use entity work.stimuliGen(stimuliGen_a);
	
    component gTickGen is  	-- generic tick generator
		generic (
        nob : natural := 16;          -- number of bits of (ticki) cycle counter
        noc : natural := 50000        -- number of (ticki) cycles
		); -- generic
		port (
        ticko    : out std_logic;     -- tick (with reduced frequency) outgoing
        ticki    : in std_logic;      -- tick/enable incoming
        clk      : in std_logic;      -- clock
        sres     : in std_logic       -- synchronous reset
		); -- port
	end component gTickGen;	
	for all : gTickGen use entity work.gTickGen(rtl);
	
    component gSISR is
        generic (
            polynom    : std_logic_vector                                  -- generator polynomial; typical it is prime polynomial
        );--}generic
        port (
            signature  : out    std_logic_vector(polynom'high downto 1);   -- data vector out/signatur/random; vectoro(msb) equals ("single-ouput") data out
		       	ticki : in std_logic;
        --    datain     : in     std_logic;                                 -- fata in
            clk        : in     std_logic;                                 -- clock
            sres       : in     std_logic           
		        
     );--}port
    end component gSISR;
    --
  	for all : gSISR use entity work.gSISR(galois);
	
	component gReg is                    -- generic tick generator
    generic (
        nob : natural := 16         -- number of bits of (ticki) cycle counter

    ); -- generic

    port (
	   datao 	: out std_logic_vector(15 downto 0);
	   datai	: in std_logic_vector(15 downto 0);
       ticki    : in std_logic;      -- tick/enable incoming
       clk      : in std_logic;      -- clock
       sres     : in std_logic       -- synchronous reset
    ); -- port

	
	end component gReg;
	for all : gReg use entity work.gReg(rtl);
	
    component display_bits is

	port (
		ticki     : in std_logic;
		clk       : in std_logic;
		sres 	   	: in std_logic;
		value     : in std_logic_vector(15 downto 0);
		seg7a	   	: out std_logic_vector(3 downto 0);
		seg7c 	   : out std_logic_vector(7 downto 0)
	);
	
	
  end component display_bits;
    	for all : display_bits use entity work.display_bits(arc);
begin 
     
    	led <= vectoro2_s(8 downto 1); 
     
	i_reg : gReg
		port map(
			ticki => tick0p5hz_s,
			clk => clk,
			sres => sres,
			datai => vectoro1_s,
			datao => vectoro2_s	
		)
	;--]i_reg
	
	display : display_bits 
		port map(
			ticki => tick1khz_s,
			clk => clk,
			sres => sres,
			value => vectoro2_s,
			seg7a => seg7a_s,
			seg7c => seg7c_s
			)
	;--]display
	
    tick1khz : gTickGen
		generic map(
			noc => 4
		)
		port map (
			ticki => '1',
			clk => clk,
			sres => sres,
			ticko => tick1khz_s
		)
	;--]tick1khz
    
	tick4hz : gTickGen
		generic map(
			noc => 5
		)
		port map (
			ticki => tick1khz_s,
			clk => clk,
			sres => sres,
			ticko => tick4hz_s
		);
	
	tick0p5hz : gTickGen
		generic map(
			noc => 8
		)
		port map (
			ticki => tick4hz_s,
			clk => clk,
			sres => sres,
			ticko => tick0p5hz_s
		)
	;--]tick0p5hz
    

    sisr_i1 : gSISR
        generic map (
            polynom   => poly
        )--]generic
        port map (
			ticki => tick4hz_s,
            signature => vectoro1_s,
			
            clk       => clk,
            sres      => sres
        )--]map
    ;--]sisr_i1
    
	stimuli : stimuliGen
		port map(
			clk => clk,
			res => sres
		)
	;--]stimuli
    
    
end architecture beh;
