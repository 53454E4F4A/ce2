onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 TICKER
add wave -noupdate /sisr_tb/tick0p5Hz/ticko
add wave -noupdate /sisr_tb/tick0p5Hz/count_cs
add wave -noupdate /sisr_tb/tick0p5Hz/ticki
add wave -noupdate /sisr_tb/tick4Hz/ticko
add wave -noupdate /sisr_tb/tick4Hz/count_cs
add wave -noupdate /sisr_tb/tick4Hz/ticki
add wave -noupdate /sisr_tb/tick1KHz/ticko
add wave -noupdate /sisr_tb/tick1KHz/count_cs
add wave -noupdate /sisr_tb/tick1KHz/ticki
add wave -noupdate /sisr_tb/stimuli/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {201 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {2530 ns}
