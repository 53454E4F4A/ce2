
library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


entity gReg is                 
    generic (
        nob : natural := 16         
    ); -- generic

    port (
	   datao 	: out std_logic_vector(15 downto 0);
	   datai	: in std_logic_vector(15 downto 0);
       ticki    : in std_logic;      -- TICK/enable Incoming
       clk      : in std_logic;      -- CLocK
       sres     : in std_logic       -- Synchronous RESET
    ); -- port

    constant mpo : natural := nob-1;  -- Most significant bit POsition


end entity gReg;


architecture rtl of gReg is

    signal data_s : std_logic_vector(15 downto 0) := (others => '0');
    signal data_cs : std_logic_vector(15 downto 0) := (others => '0');
    signal data_ns : std_logic_vector(15 downto 0);


begin


datao <= data_s;


cobilo : process(data_cs, datai, ticki) is
    variable data_v : std_logic_vector(15 downto 0) := (others => '0');
begin

	data_v := data_cs;       
	if(ticki = '1') then
		data_v := datai;				
	else			
		data_v := data_cs;      				
	end if;
		
	data_ns <= data_v;
	data_s <= data_v;	
		
end process cobilo;


sequlo : process(clk) is
begin
	if rising_edge(clk) then
		if sres = '1' then 
			data_cs <= (others => '0');		
		else		
			data_cs <= data_ns;		 
		end if; 
	end if; 
end process sequlo;


end rtl;