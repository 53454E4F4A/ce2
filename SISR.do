onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider SISR
add wave -noupdate -divider output
add wave -noupdate /sisr_tb/sisr_i1/PRBS_s
add wave -noupdate /sisr_tb/sisr_i1/signature
add wave -noupdate /sisr_tb/sisr_i1/ticki
add wave -noupdate -divider flipflops
add wave -noupdate /sisr_tb/sisr_i1/ff_ns
add wave -noupdate /sisr_tb/sisr_i1/ff_cs
add wave -noupdate /sisr_tb/sisr_i1/clk
add wave -noupdate /sisr_tb/sisr_i1/sres
TreeUpdate [SetDefaultTree]
quietly WaveActivateNextPane
WaveRestoreCursors {{Cursor 1} {1784 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 120
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {5130 ns}
