onerror {resume}
quietly WaveActivateNextPane {} 0
add wave -noupdate -divider -height 20 DUT
add wave -noupdate -divider {output PRBS}
add wave -noupdate /sisr_tb/vectorO2_s
add wave -noupdate /sisr_tb/tick0p5Hz_s
add wave -noupdate -divider PRBS
add wave -noupdate /sisr_tb/vectorO1_s
add wave -noupdate /sisr_tb/tick4Hz_s
add wave -noupdate -divider Display
add wave -noupdate /sisr_tb/seg7a_s
add wave -noupdate /sisr_tb/seg7c_s
add wave -noupdate /sisr_tb/tick1KHz_s
add wave -noupdate -divider CLocK
add wave -noupdate /sisr_tb/clk
TreeUpdate [SetDefaultTree]
WaveRestoreCursors {{Cursor 1} {193 ns} 0}
quietly wave cursor active 1
configure wave -namecolwidth 211
configure wave -valuecolwidth 100
configure wave -justifyvalue left
configure wave -signalnamewidth 0
configure wave -snapdistance 10
configure wave -datasetprefix 0
configure wave -rowmargin 4
configure wave -childrowmargin 2
configure wave -gridoffset 0
configure wave -gridperiod 1
configure wave -griddelta 40
configure wave -timeline 0
configure wave -timelineunits ns
update
WaveRestoreZoom {0 ns} {926 ns}
