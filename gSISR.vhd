library work;
    use work.all;
    
library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_unsigned.all;            
    
-- Generic Single-Input Signature Register respectively
entity gSISR is

    generic (
        polynom : std_logic_vector                                    		-- generator POLYNOMIAL; typical it is prime polynomial
    );--}generic    
    port (
        signature 	: out    	std_logic_vector(polynom'high-1 downto 0); -- signatur/random; signature(polynom'high) equals ("single-ouput"/scalar) data out
        ticki		: in 		std_logic;		 
        clk      	: in    	std_logic;                                 -- CLocK
        sres    	: in     	std_logic                                  -- Synchronous pRESet
    );--}port
      
--pragma synthesis_off vvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvvv
begin                                                                 --
    -- much too many missunderstandings                               --
    -- hence, make sure that user knows what he wants                 --
    assert not polynom'ascending                                      --
        report  "generator polynom has to be descending"              --
        severity failure;                                             --
    assert polynom'low = 0                                            --
        report  "generator polynom LSB has to be at bit position 0"   --
        severity failure;                                             --
    assert polynom'length >= 3                                        --
        report  "generator polynom LSB has to have at least 3 bits"   --
        severity failure;                                             --
    assert polynom(polynom'high)='1' and polynom(polynom'low)='1'     --
        report  "generator polynom is invalid"                        --
        severity failure;                                             --
--pragma synthesis_on ^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^
end entity gSISR;

library ieee;
use ieee.std_logic_1164.all;


architecture galois of gSISR is   
    
	signal PRBS_s : std_logic_vector(polynom'high downto 1) := (others=>'1'); -- FFs ; 
    signal ff_cs : std_logic_vector(polynom'high downto 1) := (others=>'1');  -- FFs ;
    signal ff_ns : std_logic_vector(polynom'high downto 1);
   
begin
    
    genericSISR:
    process (ticki, ff_cs) is
        variable ff_cv 	: std_logic_vector(polynom'high downto 0);
        variable ff_v 	: std_logic_vector(polynom'high downto 0);
	begin
      
      
		ff_cv(polynom'high downto 1) := ff_cs(polynom'high downto 1);
	
		if ff_cv(polynom'high downto 1) = x"FFFF" then
			ff_cv(0) := '1';
        else
			ff_cv(0) := '0';
		end if;
		
		if ticki = '1' then
			for i in polynom'high-1 downto 0 loop
				if polynom(i)='1' then
					ff_v(i+1) := ff_cv(i) xor ff_cv(polynom'high);
				else
					ff_v(i+1) := ff_cv(i);
				end if;
			end loop;
		else
			ff_v := ff_cv;
		end if;
	
	      
        
		ff_ns <= ff_v(polynom'high downto 1);
		PRBS_s  <= ff_v(polynom'high downto 1);
    end process genericSISR;
    
    
    reg:
    process ( clk, sres ) is
    begin
       if clk'event and clk='1' then
	     	 if sres='1' then
            ff_cs <= (others => '1'); 
	      	else
            ff_cs <= ff_ns;
      end if;
    end if;
    end process reg;

    signature <= PRBS_s(polynom'high downto 1);  

end architecture galois;

