 entity stimuliGen is
		port (
		clk	: out	std_logic;
		res	: out	std_logic
		);
end entity stimuligen;

	
    entity gTickGen is  	-- generic tick generator
		generic (
			nob : natural := 16;          -- number of bits of (ticki) cycle counter
			noc : natural := 50000        -- number of (ticki) cycles
		); -- generic
		port (
			ticko    : out std_logic;     -- tick (with reduced frequency) outgoing
			ticki    : in std_logic;      -- tick/enable incoming
			clk      : in std_logic;      -- clock
			sres     : in std_logic       -- synchronous reset
		); -- port
	end entity gTickGen;	
	
	
    entity gSISR is
        generic (
            polynom    : std_logic_vector                                  -- generator polynomial; typical it is prime polynomial
        );--}generic
        port (
            signature  	: out   std_logic_vector(polynom'high downto 1);   -- data vector out/signatur/random; vectoro(msb) equals ("single-ouput") data out
		    ticki		: in 	std_logic;
            clk        	: in    std_logic;                                 -- clock
            sres       	: in    std_logic           
		        
     );--}port
    end entity gSISR;
    --
  	for all : gSISR use entity work.gSISR(galois);
	
	entity gReg is                    -- register for storing current PRBS
		generic (
			nob : natural := 16        
		); -- generic

		port (
		   datao 	: out std_logic_vector(15 downto 0);
		   datai	: in std_logic_vector(15 downto 0);
		   ticki    : in std_logic;      -- tick/enable incoming
		   clk      : in std_logic;      -- clock
		   sres     : in std_logic       -- synchronous reset
		); -- port	
	end entity gReg;
	
	
    entity display_bits is
		port (
			ticki     : in std_logic;	-- ticki
			clk       : in std_logic;	-- clk
			sres 	   	: in std_logic; -- synchronous reset
			value     : in std_logic_vector(15 downto 0); -- value to be displayed
			seg7a	   	: out std_logic_vector(3 downto 0);	-- four digits 
			seg7c 	   : out std_logic_vector(7 downto 0)	-- seven segments
		);	
  end entity display_bits;