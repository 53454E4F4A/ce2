library work;
    use work.all;

library ieee;
    use ieee.std_logic_1164.all;
    use ieee.std_logic_arith.all;
    use ieee.std_logic_unsigned.all;


entity gTickGen is                    -- Generic TICK GENerator
    generic (
        nob : natural := 16;          -- Number Of Bits of (ticki) cycle counter
        noc : natural := 50000        -- Number Of (ticki) Cycles
    ); -- generic

    port (
        ticko    : out std_logic;     -- TICK (with reduced frequency) Outgoing
        ticki    : in std_logic;      -- TICK/enable Incoming
        clk      : in std_logic;      -- CLocK
        sres     : in std_logic       -- Synchronous RESET
    ); -- port

    constant mpo : natural := nob-1;  -- Most significant bit POsition
    constant max : natural := noc-1;  -- MAXimum value of counter

end entity gTickGen;


architecture rtl of gTickGen is

    signal ticko_s	 	: std_logic := '0';
    signal count_cs 	: std_logic_vector(mpo downto 0) := (others => '0');
    signal count_ns 	: std_logic_vector(mpo downto 0);


begin

ticko <= ticko_s;


cobilo : process(count_cs, ticki) is

    variable ticko_v : std_logic := '0';
    variable count_v : std_logic_vector(mpo downto 0) := (others => '0');

begin

	ticko_v := '0';
	count_v := count_cs;
       
	if(ticki = '1') then
		if(count_cs = max) then			
			count_v := (others => '0'); 
			ticko_v := '1'; 				
		else			
			ticko_v := '0';
			count_v := (count_cs + 1);           				
		end if;
	end if;		
	ticko_s <= ticko_v;
	count_ns <= count_v;
		
		
end process cobilo;


sequlo : process(clk) is
begin
	if rising_edge(clk) then
		if sres = '1' then 
			count_cs <= (others => '0');
		else		
			count_cs <= count_ns;		 
		end if; 
	end if; 
end process sequlo;

end rtl;